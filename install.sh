#!/bin/sh
# Stop on error
set -e
# Install mandatory programs and utilities
apk add bash rsync openssh openssl make git sed
