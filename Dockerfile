ARG VERSION=latest

FROM hashicorp/terraform:${VERSION}

COPY install.sh /root/
RUN /bin/sh /root/install.sh
ENTRYPOINT []

